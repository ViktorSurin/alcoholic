<div class="container center-block">
    <div class="row">
        <div class="col">
            <form action="index.php?page=game1" method="post" class="form-inline">
                <div class="form-group mx-sm-3 mb-2">
                    <label for="name" class="sr-only">Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                </div>
                <button type="submit" class="btn btn-primary mb-2">GO</button>
            </form>
        </div>
    </div>
</div>