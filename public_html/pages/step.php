<div class="container step-page">
    <div class="row w-100">
        <div class="col">
            <div class="container">
                <div class="row">
                    <div class="col d-flex justify-content-end">
                        <div class="form-row">
                            <div class="col">
                                <button id="loadSave" class="btn btn-primary">Загрузить</button>
                            </div>
                            <div class="col">
                                <button id="save_game" class="btn btn-primary">Сохранить</button>
                            </div>
                            <div class="col">
                                <a href="/">
                                    <button class="btn btn-secondary">Начать игру заного</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container contant">
                <div class="row">
                    <div class="col-6">
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <form method="post" id="game_form">
                                        <label for="number">Введите цыфру от 1 до 5</label>
                                        <div>
                                            <input type="number" name="number" id="number" min="1" max="5"
                                                   class="form-control" data-bind="value:replyNumber">
                                        </div>
                                        <div>
                                            <input type="button" id="submit__form_step" value="Сделать ход"
                                                   class="btn btn-primary button_step">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div id="list_users">
                            <?php
                                require_once __DIR__ . '/view/partials/list_user.php';
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>