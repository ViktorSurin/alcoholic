<div class="container history-page">
    <div class="row w-100">
        <div class="col">
            <div class="d-flex justify-content-end">
                <a href="/">
                    <button class="btn btn-secondary history-button">Начать игру заного</button>
                </a>
            </div>
            <ul class="list-group">
                <?php foreach ($result as $key => $items) { ?>
                    <li class="list-group-item text-center">Step <?= ++$key; ?></li>
                    <?php foreach ($items as $item) { ?>
                        <li class="list-group-item"><?= $item; ?></li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>