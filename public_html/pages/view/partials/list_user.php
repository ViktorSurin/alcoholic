<?php
    $users = getUsers();
?>
<?php if (count($users) > 0) { ?>
<ul class="list-group">
    <li class="list-group-item active">
        <div class="container">
            <div class="row">
                <?php if (empty($_SESSION['hasUseFile']) ) {?>
                    <div class="col">
                        <div class="custom-file">
                            <form action="/index.php?action=load_file" id="form_user_file" enctype="multipart/form-data" method="post">
                                <input name="userfile" type="file" id="userfile" class="custom-file-input"/>
                                <label class="custom-file-label" for="userfile">Заюзать баф</label>
                            </form>
                        </div>
                    </div>
                <?php } ?>
                <?php if (empty($_SESSION['hasMedical'])) { ?>
                    <div class="col">
                        <button id="medical" class="btn btn-warning">Заюзать Аптечку</button>
                    </div>
                <?php } ?>
            </div>
            <div class="row">
                <div class="col title-list-user">
                    Список игроков
                </div>
            </div>
        </div>
    </li>
    <?php
        foreach ($users as $user) {
            echo '<li class="list-group-item">'.$user->getName().' --- '.$user->getHp().'</li>';
        }
    ?>
</ul>
<?php } ?>