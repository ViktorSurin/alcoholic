<?php

class Game {
    private $name;
    private $hp = 20;

    public function __construct($name = 'Bot')
    {
       $this->setName($name);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getHp()
    {
        return $this->hp;
    }

    /**
     * @param mixed $hp
     */
    public function setHp($hp)
    {
        $this->hp = $hp;
    }

    /**
     * @param string $type
     */
    public function minusHp($type = 'bot')
    {
        $hp = $this->getHp();
        if ($type == 'user') {
            $hp = $hp - rand(1, 4);
            $this->setHp($hp);
        } else {
            $hp = $hp - 3;
            $this->setHp($hp);
        }
    }
}