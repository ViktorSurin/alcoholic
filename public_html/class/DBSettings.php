<?php

/**
 * Class DatabaseSettings
 */
class DatabaseSettings
{
    var $settings;

    /**
     * @var $settings['dbhost'] Host name
     * @var $settings['dbname'] Database name
     * @var $settings['dbusername'] Username. Where default value = '';
     * @var $settings['dbpassword'] Password. Where default value = '';
     *
     * @return mixed
     */
    function getSettings()
    {
        $settings['dbhost'] = 'localhost';
        $settings['dbname'] = 'alcoholic.loc';
        $settings['dbusername'] = 'root';
        $settings['dbpassword'] = '12345';

        return $settings;
    }
}