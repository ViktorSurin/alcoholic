<?php

require_once( 'DBSettings.php' );

/**
 * Class DBClass to connect to database and fire queries
 */
class DBClass extends DatabaseSettings
{
    var $classQuery;
    var $link;

    var $errno = '';
    var $error = '';

    /**
     * Connects to the database
     *
     * DBClass constructor.
     */
    function __construct()
    {
        // Load settings from parent class
        $settings = DatabaseSettings::getSettings();

        // Get the main settings from the array we just loaded
        $host = $settings['dbhost'];
        $name = $settings['dbname'];
        $user = $settings['dbusername'];
        $pass = $settings['dbpassword'];

        // Connect to the database
        $this->link = new mysqli( $host , $user , $pass , $name );
    }

    /**
     * Executes a database query
     *
     * @param $query
     *
     * @return bool|mysqli_result
     */
    function query( $query )
    {
        $this->classQuery = $query;
        return $this->link->query( $query );
    }

    /**
     * @param $query
     *
     * @return string
     */
    function escapeString( $query )
    {
        return $this->link->escape_string( $query );
    }

    /**
     * Get the data return int result
     *
     * @param $result
     *
     * @return mixed
     */
    function numRows( $result )
    {
        return $result->num_rows;
    }

    /**
     * @return mixed
     */
    function lastInsertedID()
    {
        return $this->link->insert_id;
    }

    /**
     * Get query using assoc method
     *
     * @param $result
     *
     * @return mixed
     */
    function fetchAssoc( $result )
    {
        return $result->fetch_assoc();
    }

    /**
     * Gets array of query results
     *
     * @param $result
     * @param int $resultType
     *
     * @return mixed
     */
    function fetchArray( $result , $resultType = MYSQLI_ASSOC )
    {
        return $result->fetch_array( $resultType );
    }

    /**
     * Fetches all result rows as an associative array, a numeric array, or both
     *
     * @param $result
     * @param int $resultType
     *
     * @return mixed
     */
    function fetchAll( $result , $resultType = MYSQLI_ASSOC )
    {
        return $result->fetch_all( $resultType );
    }

    /**
     * Get a result row as an enumerated array
     *
     * @param $result
     *
     * @return mixed
     */
    function fetchRow( $result )
    {
        return $result->fetch_row();
    }

    /**
     * Free all MySQL result memory
     *
     * @param $result
     */
    function freeResult( $result )
    {
        $this->link->free_result( $result );
    }

    /**
     * Closes the database connection
     */
    function close()
    {
        $this->link->close();
    }

    /**
     * @return string
     */
    function sql_error()
    {
        if( empty( $error ) ) {
            $errno = $this->link->errno;
            $error = $this->link->error;
        }

        return $errno . ' : ' . $error;
    }
}