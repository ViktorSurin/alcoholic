$(function () {
    $('#submit__form_step').on('click', function() {
        $.ajax({
            type: 'POST',
            url: '/index.php?action=step',
            data: $('#game_form').serialize(),
            success: function(msg) {
                if (msg == 'Game Over') {
                    window.location = '/index.php?module=games&page=game1over';
                } else {
                    $('#list_users').html(msg);
                }
            }
        });
    });

    $('body').on('change', '#userfile', function() {
        var file_data = this.files[0];
        var form_data = new FormData();
        form_data.append('file', file_data);
        $.ajax({
            url: '/index.php?action=load_file', // point to server-side PHP script
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(msg){
                $('#list_users').html(msg);
                $('#form_user_file').hide();
            }
        });
    });

    $('body').on('click', '#save_game', function() {
        $.ajax({
            type: 'GET',
            url: '/index.php',
            data: {
                action: 'save'
            },
            success: function(msg){

            }
        });
    });

    $('body').on('click', '#medical', function() {
        $.ajax({
            type: 'GET',
            url: '/index.php',
            data: {
                action: 'medical'
            },
            success: function(msg) {
                $('#list_users').html(msg);
            }
        });
    });

    $('#number').keydown(function () {
        // Save old value.
        if (!$(this).val() || (parseInt($(this).val()) <= 5 && parseInt($(this).val()) >= 0))
            $(this).data('old', $(this).val());
    });
    $('#number').keyup(function () {
        // Check correct, else revert back to old value.
        if (!$(this).val() || (parseInt($(this).val()) <= 5 && parseInt($(this).val()) >= 0))
            ;
        else
            $(this).val($(this).data('old'));
    });

    $('body').on('click', '#loadSave', function() {
        $.ajax({
            url: '/index.php',
            method: 'GET',
            data: {
                action: 'loadSave'
            },
            success: function (msg) {
                $('#list_users').html(msg);
            }
        });
    });
});