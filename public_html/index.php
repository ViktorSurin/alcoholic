<?php

global $game;

session_start();

require_once __DIR__ . '/class/DBClass.php';
require_once __DIR__ . '/function.php';
require_once __DIR__ . '/class/game.php';

if (!$_GET) {
    require_once 'pages/view/header.php';
    require_once 'pages/main.php';
    require_once 'pages/view/footer.php';
    session_destroy();
} elseif (isset($_GET['page']) && $_GET['page']  == 'game1') {
    initGames();
    require_once 'pages/view/header.php';
    require_once 'pages/step.php';
    require_once 'pages/view/footer.php';
} elseif (isset($_GET['action']) && $_GET['action'] == 'step') {
    $users = unserialize($_SESSION['users']);

    if ((int)$_REQUEST['number'] === (int)rand(1,7)) {
        $idBot = rand(1, count($users) - 1);
        $users[$idBot]->minusHp();
    } else {
        $users[0]->minusHp('user');
    }

    $_SESSION['users'] = serialize($users);

    foreach ($users as $user) {
        if ($user->getHp() <= 0) {
            echo 'Game Over';

            return;
        }
    }

    require_once __DIR__ . '/pages/view/partials/list_user.php';

    saveHistory();
} elseif (isset($_GET['action']) && $_GET['action'] == 'load_file') {
    if (isset($_FILES['file'])) {
        $fp = fopen($_FILES['file']['tmp_name'], 'r');
        $line = fgets($fp);
        if (trim($line) == 'power strike' && empty($_SESSION['hasUseFile'])) {
            $users = unserialize($_SESSION['users']);
            $idUser = rand(0, count($users) - 1);
            $hp = $users[$idUser]->getHp() + 5;
            $users[$idUser]->setHp($hp);
            $_SESSION['users'] = serialize($users);
            $_SESSION['hasUseFile'] = true;
        }
    }

    require_once __DIR__ . '/pages/view/partials/list_user.php';
} elseif (isset($_GET['action']) && $_GET['action'] == 'save') {
    saveGame();
}  elseif (isset($_GET['action']) && $_GET['action'] == 'medical') {
    if (empty($_SESSION['hasMedical'])) {
        $users = unserialize($_SESSION['users']);
        $idUser = rand(0, count($users) - 1);
        $hp = $users[$idUser]->getHp() + 5;
        $users[$idUser]->setHp($hp);
        $_SESSION['users'] = serialize($users);
        $_SESSION['hasMedical'] = true;

        require_once __DIR__ . '/pages/view/partials/list_user.php';
    }
} elseif (isset($_GET['page']) && $_GET['page'] == 'game1over') {
    require_once 'pages/view/header.php';
    $result = getHistory();
    require_once __DIR__ . '/pages/view/partials/list_history.php';
    require_once 'pages/view/footer.php';
} elseif (isset($_GET['action']) && $_GET['action'] == 'loadSave') {
    loadSave();
    require_once __DIR__ . '/pages/view/partials/list_user.php';
}