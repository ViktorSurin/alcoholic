<?php

function getUsers()
{
    $users = empty($_SESSION['users']) ? [] : unserialize($_SESSION['users']);
    return $users;
}

function getHistory()
{
    $DB = new DBClass;
    $result = $DB->query('
        SELECT game_session.id, game_session.title, game_session.user, game_history.value 
        FROM game_session 
        LEFT JOIN game_history ON game_session.id = game_history.session_id  
        WHERE game_session.id='.$_SESSION['game_id']
    );

    $i = $result->num_rows;

    $history = [];
    while ($i--) {
        $data = $result->fetch_assoc();
        $data = unserialize($data['value']);
        if (!empty($data)) {
            foreach ($data as $value) {
                $history[$i][] = $value->getName() .' --- '.$value->getHp();
            }
        } else {
            header('Location: /');
        }
    }

    return $history;
}

function initGames()
{
    $_SESSION['name'] = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';
    $_SESSION['game_name'] = 'gameName_'.rand(1, 15);

    $DB = new DBClass;
    $DB->query("INSERT INTO game_session (title, user) VALUES ('Game', '".$_SESSION['game_name']."')");
    $_SESSION['game_id'] = $DB->lastInsertedID();

    $users[] = new Game($_SESSION['name']);
    for ($i = 0; $i < 5; $i++) {
        $users[] = new Game;
    }

    $_SESSION['users'] = serialize($users);
}

function saveHistory()
{
    $DB = new DBClass;
    $DB->query("INSERT INTO game_history (session_id, value) VALUES ('".$_SESSION['game_id']."', '".$_SESSION['users']."')");
    $_SESSION['last_history_id'] = $DB->lastInsertedID();
}

function saveGame()
{
    $medical = empty($_SESSION['hasMedical']) ? 'false' : 'true';
    $hasUseFile = empty($_SESSION['hasUseFile']) ? 'false' : 'true';

    $DB = new DBClass;
    $DB->query("INSERT INTO game_save (id_history, medical, has_file) VALUES ('".$_SESSION['last_history_id']."', '$medical', '$hasUseFile')");
    $_SESSION['id_save'] = $_SESSION['last_history_id'];
}

function loadSave()
{
    $DB = new DBClass;
    $result = $DB->query("
        SELECT * FROM game_history
        LEFT JOIN game_save ON game_save.id_history = game_history.id
        WHERE game_history.id =".$_SESSION['id_save']);

    $data = $result->fetch_assoc();

    $_SESSION['users'] = $data['value'];

    if ($data['medical'] == 'false') {
         unset($_SESSION['hasMedical']);
    }

    if ($data['has_file'] == 'false') {
        unset($_SESSION['hasUseFile']);
    }
}

function dump($data = '')
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}